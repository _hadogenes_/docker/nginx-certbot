FROM nginx

RUN DEBIAN_FRONTEND=noninteractive apt update \
  && DEBIAN_FRONTEND=noninteractive apt install -y \
    supervisor \
    cron \
    certbot \
    python3-certbot-nginx \
  && DEBIAN_FRONTEND=noninteractive apt autoremove --purge \
  && DEBIAN_FRONTEND=noninteractive apt clean \
  && rm -rf /var/lib/apt/lists/*

COPY supervisord/*.conf /etc/supervisor/conf.d/
CMD [ "/usr/bin/supervisord" ]
